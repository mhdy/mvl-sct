# xsct

[xsct](https://github.com/faf0/sct/) (X11 set color temperature) is a UNIX tool which allows to set the screen's color temperature.
